//Momshad Hussain	
package Casino;
	import javafx.application.*;
	import javafx.event.ActionEvent;
	import javafx.event.EventHandler;
	import javafx.stage.*;
	import javafx.scene.*;
	import javafx.scene.paint.*;
	import javafx.scene.control.*;
	import javafx.scene.layout.*;
	
	import javafx.event.ActionEvent;
	
	public class DiceRoll extends Application {
		
		public void start(Stage stage) {
			DiceGame dg = new DiceGame(250);
			Group root = new Group(); 
			HBox button = new HBox();
			HBox textFields = new HBox();
			VBox vbox = new VBox();
			Button even = new Button("Even");
			Button odd = new Button("Odd");
			TextField welcome = new TextField("Even or Odd ? Enter a bet :");
			TextField bet = new TextField("");
			TextField balance = new TextField("Wallet : " +dg.getWallet());
			textFields.getChildren().addAll(welcome,bet,balance);
			button.getChildren().addAll(even,odd);
			vbox.getChildren().addAll(textFields,button);
			root.getChildren().add(vbox);
			welcome.setPrefWidth(500);
			bet.setPrefWidth(200);
			balance.setPrefWidth(200);
			
	
			 odd.setOnAction(new EventHandler<ActionEvent>() {
		          public void handle(ActionEvent e) {
		         	int betAmount = Integer.parseInt(bet.getText());
		         	DiceRollChoice betObjOdd = new DiceRollChoice("odd",dg,balance,welcome,bet,betAmount);
					odd.setOnAction(betObjOdd);
		                
		            } 
		       }
		      );
			 
			 even.setOnAction(new EventHandler<ActionEvent>() {
		          public void handle(ActionEvent e) {
		         	int betAmount = Integer.parseInt(bet.getText());
		        	DiceRollChoice betObjOdd = new DiceRollChoice("even",dg,balance,welcome,bet,betAmount);
					even.setOnAction(betObjOdd);
		                
		            } 
		       }
		      );
			
			//DiceRollChoice betObjOdd = new DiceRollChoice("odd",dg,balance,welcome,bet,betAmount);
			//odd.setOnAction(betObjOdd);
			
			//DiceRollChoice betObjEven = new DiceRollChoice("even",dg,balance,welcome,bet,betAmount);
			//even.setOnAction(betObjEven);
		
			//scene is associated with container, dimensions
			Scene scene = new Scene(root, 650, 300); 
			scene.setFill(Color.RED);

			//associate scene to stage and show
			stage.setTitle("Rock Paper Scissors"); 
			stage.setScene(scene); 
			
			stage.show(); 
	
	}
		  public static void main(String[] args) {
		        Application.launch(args);
		  }
}

