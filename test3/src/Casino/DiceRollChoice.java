//Momshad Hussain
package Casino;
import javafx.scene.control.*;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class DiceRollChoice implements EventHandler<ActionEvent>{
	private String choice;
	private TextField bet;
	private DiceGame dg;
	private TextField wallet;
	private TextField welcome;
	private int betAmount;
	
	
	public DiceRollChoice(String choice,DiceGame dg,TextField wallet, TextField welcome,TextField bet, int betAmount ) {
	 this.choice = choice;
	 this.bet = bet;
	 this.dg = dg ;
	 this.wallet = wallet ;
	 this.welcome = welcome;
	 this.betAmount = betAmount;
	}


	@Override
	public void handle(ActionEvent e) {
		
		//int betAmount = Integer.parseInt(bet.getText());
		String Result = dg.playround(this.choice,this.betAmount);
		this.welcome.setText(Result);
		this.wallet.setText("Wallet : " +this.dg.getWallet());
		
	}
}
