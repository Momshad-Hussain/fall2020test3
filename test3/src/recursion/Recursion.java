package recursion;
//Momshad Hussain
import java.util.Arrays;

public class Recursion {
	private static int numberOfMatches = 0;
	private static int currentIndex = 0;
	
	public static void main(String[] args) {
		int [] numbers = {1,12,3,4,5,6,7,5,8,9,5,6,5,6,5,9,4,5,6,2,1,4,5,6,12,4,5,6,2};
		int i = 13;
		System.out.println(recursiveCount(numbers,i));

	}

    public static int recursiveCount(int[] numbers, int n) {
		int sizeOfArray = numbers.length;
		if(isMatch(numbers[sizeOfArray-1],currentIndex, n)) {
			numberOfMatches++;
		}
		
		currentIndex++;
		
		if(sizeOfArray == 1) {
			return numberOfMatches;
		}
		else {
			return recursiveCount(
					Arrays.copyOf(numbers, numbers.length-1), n
					);
		}
		
 } 


    private static boolean isMatch(int currentInt, int elementIndex, int n) {
    	if(currentInt < 10 ) {
    		return true;
    	}
    	else if( elementIndex == 0 || elementIndex % 2 == 1) {
    		if(currentInt >= n) {
    			return true;
    		}
    	}
    	
    	return false;
    }
}
